#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi

#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > cc2dc_exci.info <<%EOF%
   cc2dc_exci  
   --------------
   Molecule:         H2O in a spherical cavity of radius 4.00 au
   Wave Function:    CC2 / cc-pVDZ
   Test Purpose:     Check energy, dipole moment, the first four 
                     excitation energies and transition properties.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > cc2dc_exci.mol <<%EOF%
BASIS
cc-pVDZ
H2O i H2O(DC)
------------------------
    2    0         1 1.00D-12
        8.0   1
O            0.000000        0.000000        0.000000
        1.0   2
H           -0.756799        0.000000        0.586007
H            0.756799        0.000000        0.586007
%EOF%
#
#######################################################################
#  DALTON INPUT
#######################################################################
cat > cc2dc_exci.dal <<%EOF%
**DALTON INPUT
.RUN WAVEFUNCTION
**INTEGRALS
.DIPLEN
*ONEINT
.SOLVENT
 10
**WAVE FUNCTIONS
.CC
*SCF INPUT
.THRESH
1.0D-11
*CC INP
.CC2
.THRLEQ
 1.0D-7
.THRENR
 1.0D-7
.MAX IT
 90
.MXLRV
 60
*CCSLV
.SOLVAT
 1
10 4.00  78.54  1.778
.ETOLSL
 1.0D-6
.TTOLSL
 1.0D-6
.LTOLSL
 1.0D-6
.MXSLIT
 60
*CCFOP
.DIPMOM
.NONREL
*CCEXCI
.NCCEXCI
 4 0 0 0
.THREXCI
 1.0D-7
*CCLRSD
.DIPOLE
**END OF DALTON INPUT
%EOF%
#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >cc2dc_exci.check
cat >>cc2dc_exci.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

if $GREP -q "not implemented for parallel calculations" $log; then
   echo "TEST ENDED AS EXPECTED"
   exit 0
fi

# Total energy and solvation energy compared:
CRIT1=`$GREP "CC2      Total energy:                 -76\.2386472." $log | wc -l`
CRIT2=`$GREP "CC2      Solvation energy:              ( \-|\-0)\.0076650." $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2`
CTRL[1]=2
ERROR[1]="ENERGY TERMS NOT CORRECT"

# Dipole moment components compared:
CRIT1=`$GREP "z * ( |0)\.84595673 * 2\.150207.." $log | wc -l`
TEST[2]=`expr $CRIT1`
CTRL[2]=1
ERROR[2]="DIPOLE MOMENT NOT CORRECT"


# The first four excitation energies compared:
CRIT1=`$GREP "1A * \| * 1 * \| * ( |0)\.316317. * \| * 8\.6074. * \| * 69423\.64." $log | wc -l`
CRIT2=`$GREP "1A * \| * 2 * \| * ( |0)\.399128. * \| * 10\.8608. * \| * 87598\.52." $log | wc -l`
CRIT3=`$GREP "1A * \| * 3 * \| * ( |0)\.411314. * \| * 11\.1924. * \| * 90273\.01." $log | wc -l`
CRIT4=`$GREP "1A * \| * 4 * \| * ( |0)\.496567. * \| * 13\.5122. * \| * 108983\.97." $log | wc -l`
TEST[3]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4`
CTRL[3]=4
ERROR[3]="EXCITATION ENERGIES NOT CORRECT"

# The first four excitation energies compared:
CRIT1=`$GREP "1A * \| * 1 * \| * 0*\.1447848 * \| * 0*\.0305320 * \| * Y" $log | wc -l`
CRIT2=`$GREP "1A * \| * 2 * \| * \-*0*\.0000000 * \| * \-*0*\.0000000 * \| * -" $log | wc -l`
CRIT3=`$GREP "1A * \| * 3 * \| * 0*\.3729071 * \| * 0*\.1022546 * \| * Z" $log | wc -l`
CRIT4=`$GREP "1A * \| * 4 * \| * 0*\.2619028 * \| * 0*\.0867016 * \| * X" $log | wc -l`
TEST[4]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4`
CTRL[4]=4
ERROR[4]="TRANSITION PROPERTIES NOT CORRECT"

PASSED=1
for i in 1 2 3 4
do 
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo "${ERROR[i]} ( test = ${TEST[i]}; control = ${CTRL[i]} ); "
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

%EOF%
chmod +x cc2dc_exci.check
#######################################################################
