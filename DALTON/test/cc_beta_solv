#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > cc_beta_solv.info <<'%EOF%'
   cc_beta_solv
   -----------
   Molecule:         H2S in C2v
   Wave Function:    CC2/CCSD / 6-31G
   Test Purpose:     Solvent for CC2/CCSD 1. hyperpolarizability 
%EOF%

#######################################################################
#  INTEGRAL INPUT
#######################################################################
cat > cc_beta_solv.mol <<'%EOF%'
BASIS
6-31G
Test solvent for CCSD excitation energies
and various properties
    2    2  X  Y        1.D-17
      16.     1    3    1    1    1
S#1   0.0000000000000000  0.0000000000000000 -0.1947465500000000       *
        1.    1    2    1    1
H#2   1.8082633700000000  0.0000000000000000  1.5453841700000000       *
%EOF%


#######################################################################
#  DALTON INPUT
#######################################################################
cat > cc_beta_solv.dal <<'%EOF%'
**DALTON
.RUN WAVE FUNCTIONS
**INTEGRALS
.DIPLEN
.DIPVEL
*ONEINT
.SOLVENT
 10
**WAVE FUNCTIONS
.CC
*SCF INP
.THRESH
1.0D-9
*CC INP
.CC2
.CCSD
.FREEZE
 5 0
.THRLEQ
 1.0D-9
.THRENR
 1.0D-12
.PRINT
 1
.MAX IT
 80
*CCSLV
.SOLVAT
 1
 10 4.53  20.7   1.841
.ETOLSL
 1.0D-10
.TTOLSL
 1.0D-06
.LTOLSL
 1.0D-06
.MXSLIT
 50
*CCFOP
.DIPMOM
.NONREL
*CCQR
.DIPOLE
.SHGFRE
 1
 0.0040
**END OF DALTON INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >cc_beta_solv.check
cat >>cc_beta_solv.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

if $GREP -q "not implemented for parallel calculations" $log; then
   echo "TEST ENDED AS EXPECTED"
   exit 0
fi

# Solvation energies
# Solvent: L_max=10, R_cav=  4.5300, Eps_st = 20.7000, Eps_op =  1.8410:
#            CC2      Total energy:                -398.6893746207
# Solvent: L_max=10, R_cav=  4.5300, Eps_st = 20.7000, Eps_op =  1.8410:
#            CCSD     Total energy:                -398.7042657891
CRIT1=`$GREP "CC2      Total energy: *\-398.6893746" $log | wc -l`
CRIT2=`$GREP "CCSD     Total energy: *\-398.7042657" $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2 ` 
CTRL[1]=2
ERROR[1]="ENERGIES NOT CORRECT"

#
# Test of CC2 hyperpolarizabilities
#
CRIT1=`$GREP "XDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * -38\.111801" $log | wc -l`
CRIT2=`$GREP "XDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * -38\.111801" $log | wc -l`
CRIT3=`$GREP "YDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * YDIPLEN * \(unrel\.\) * ( |0)\.0040 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * -1\.3835495" $log | wc -l`
CRIT4=`$GREP "YDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * YDIPLEN * \(unrel\.\) * ( |0)\.0040 * -1\.3835495" $log | wc -l`
CRIT5=`$GREP "ZDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * -38\.108467" $log | wc -l`
CRIT6=`$GREP "ZDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * YDIPLEN * \(unrel\.\) * ( |0)\.0040 * YDIPLEN * \(unrel\.\) * ( |0)\.0040 * -1\.3777907" $log | wc -l`
CRIT7=`$GREP "ZDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * -20\.108137" $log | wc -l`
TEST[2]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 `
CTRL[2]=7
ERROR[2]="CC2 FIRST HYPERPOLARIZABILITY NOT CORRECT"

#
# Test of CCSD hyperpolarizabilities
#
CRIT1=`$GREP "XDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * -31\.953657" $log | wc -l`
CRIT2=`$GREP "XDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * -31\.953657" $log | wc -l`
CRIT3=`$GREP "YDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * YDIPLEN * \(unrel\.\) * ( |0)\.0040 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * -1\.6265789" $log | wc -l`
CRIT4=`$GREP "YDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * YDIPLEN * \(unrel\.\) * ( |0)\.0040 * -1\.6265789" $log | wc -l`
CRIT5=`$GREP "ZDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * -31\.950369" $log | wc -l`
CRIT6=`$GREP "ZDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * YDIPLEN * \(unrel\.\) * ( |0)\.0040 * YDIPLEN * \(unrel\.\) * ( |0)\.0040 * -1\.6206692" $log | wc -l`
CRIT7=`$GREP "ZDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * -21\.083406" $log | wc -l`
TEST[3]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 `
CTRL[3]=7
ERROR[3]="CCSD FIRST HYPERPOLARIZABILITY NOT CORRECT"


PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo "${ERROR[i]} ( test = ${TEST[i]}; control = ${CTRL[i]} ); "
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi                
%EOF%
chmod +x cc_beta_solv.check
#######################################################################
